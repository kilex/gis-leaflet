<!DOCTYPE html>
<html>
<head>
    <title>Upload GPX track</title>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
</head>
<body>
<div class="container">
    <div class="page-header">
<?php
/**
 * Created by PhpStorm.
 * User: kilex
 * Date: 07.07.14
 * Time: 20:54
 */

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

$uploaddir = './uploads/';
$randomname=random_string(6);
//$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
$uploadfile = $uploaddir . basename($randomname.'.gpx');
$url='http://gis.kilex.ru?gpx='.$randomname;
//print $uploadfile;

if ($_FILES['userfile']['type']==='application/octet-stream') {
    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
        echo "<h3>Файл был успешно загружен. <small>Ссылка доступа - <a href='$url'>$url</a></small></h3>";
    } else {
        echo "<h3>Хрень какаято</h3>";
    }
}
else {
    print "<h3 class='alert alert-danger'>Мы принимаем только .gpx треки</h3>";
}

//echo 'Некоторая отладочная информация:';
//print_r($_FILES);
?>
        </div>
    <p><a href="/">Вернутся на карту</a> или <a href="upload.html">загрузку трека</a></p>
</div>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-30095391-3', 'auto');
    ga('send', 'pageview');

</script>
</body>